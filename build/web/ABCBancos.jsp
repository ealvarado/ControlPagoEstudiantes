<%-- 
    Document   : ABCBancos
    Created on : 9/10/2018, 09:26:12 PM
    Author     : Alienware
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
    String head = request.getSession().getAttribute("head").toString();
    out.println(head);
%>
<div class="content mt-3">
    <div class="row">
        <div class="col-12">
            <div class="col-6">
                <button 
                    type="button" 
                    class="btn-lg btn-success" 
                    onclick="InsertaBanco()"
                    >Agregar Banco <i class="fa fa-save"></i>
                </button>
                <button 
                    type="button" 
                    class="btn-lg btn-primary modificabtn" 
                    onclick="ModificaBanco()"
                    style="display: none;"
                    >Modificar Banco <i class="fa fa-male"></i>
                </button>
                <button 
                    type="button" 
                    class="btn-lg btn-danger modificabtn"
                    onclick="CancelaModificacion()"
                    style="display: none;"
                    >Cancelar Operación<i class="fa fa-times-circle"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="row">
        <div class="col-12">
            <div class="col-2">
                <div class="form-group">
                    <label for="usr">Nombre Banco:</label>
                    <input type="text" class="form-control form-control-sm letras limpia" id="banco">
                </div>
            </div>
            <!--div class="col-2">
                <div class="form-group">
                    <label for="usr">Sección:</label>
                    <input type="text" class="form-control form-control-sm letras limpia" id="seccion" maxlength="1">
                </div>
            </div!-->
        </div>
    </div>
    <div class="row">
        <div class="col-12 table-responsive" class="">
            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                <thead class="thead-dark">
                  <tr>
                    <th>No.</th>
                    <th>Banco</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody id="tbodyBanco"></tbody>
            </table>    
        </div>
    </div>
    <div class="row">
    </div>
</div>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function(){
        <%String body = request.getSession().getAttribute("tbodyBanco").toString();%>
        var cuerpotabla = "<% out.print(body);%>";
        $("#tbodyBanco").html(cuerpotabla);
        $('.letras').keyup(function (){
            this.value = (this.value + '').replace(/([^ a-z-A-z])/g, '').toUpperCase();
        });
        $('.numeros').keyup(function (){
            this.value = (this.value + '').replace(/([^0-9])/g, '');
        });
        $('#banco').focus();
        EliminarBanco();
        EditaBanco();
        IDBanco=0;
    });
    
    function ValidaCampos(banco){
        var dato=false;
        if(banco == null || banco == ''){
            swal({
               title: "Faltan datos por ingresar!",
               text: "Ingrese el nombre del banco.",
               icon: "info",
               button: "Aceptar"
             }).then(() => {
               $("#grado").focus();
             });
        }else{
            dato=true;
        }
        return dato;
    }
    
    function InsertaBanco(){
        var banco = $("#banco").val();
        
        var valida = ValidaCampos(banco);
        
        if(valida == true){
            $.ajax({
                type: "POST",
                url: "ABCBancosAjax",
                data: {
                    f:1, 
                    banco : banco
                },
                success: function(data){
                    if(data == "200"){
                        swal({
                            title: "Ingreso Correcto!",
                            text: "Banco Ingresado Correctamente.",
                            icon: "success",
                            button: "Aceptar",
                          });
                          tabla();
                    }
                    else{
                        swal({
                            title: "Ingreso Fallido!",
                            text: "El banco no se pudo ingresar.",
                            icon: "error",
                            button: "Aceptar",
                          });
                    }
                }
            });
        }
    }
    
    function tabla(){
        $.ajax({
                type: "POST",
                url: "ABCBancosAjax",
                data: {f:2},
                success: function(data){
                    $('.limpia').val('');
                    $("#tbodyBanco").html(data);
                    EliminarBanco();
                    EditaBanco();
                }
            });
    }
    function EliminarBanco(){
            $('.elimina').click(function(e){
                var id = e.target.id;
                swal({
                        title: "Esta seguro de eliminar este banco?",
                        text: "Toda la información referente a este banco sera borrada",
                        icon: "warning",
                        buttons: true,
                        buttons: ["Cancelar", "Aceptar"],
                        dangerMode: true,
                        
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: "POST",
                            url: "ABCBancosAjax",
                            data: {f:3, id: id},
                            success: function(data){
                                if(data == "200"){
                                    swal("Banco eliminado correctamente!", {
                                        icon: "success",
                                        button: "Aceptar",
                                    });
                                    tabla();
                                }
                                else{
                                    swal({
                                        title: "Operación Fallida!",
                                        text: "El banco no se pudo eliminar.",
                                        icon: "error",
                                        button: "Aceptar",
                                    });
                                }
                            }
                        });
                    } else {
                        swal({
                            text: "Operación cancelada!",
                            button: "Aceptar",
                        });
                    }
                });
            })
        }
        
        function EditaBanco(){
            $('.modifica').click(function(e){
                $('.btn-success').hide();
                $('.modificabtn').show();
                var id = e.target.id;
                var banco = $('#'+id).data('banco');
                IDBanco = $('#'+id).data('idbanco');
                
                $("#banco").val(banco);
                $("#banco").focus();
            })
        }
        
        function CancelaModificacion(){
            $('.limpia').val('');
            $('.btn-success').show();
            $('.modificabtn').hide();
        }
        
        function ModificaBanco(){
            var banco = $("#banco").val();
            var valida = ValidaCampos(banco);

            if(valida == true){
                $.ajax({
                    type: "POST",
                    url: "ABCBancosAjax",
                    data: {
                        f:4,
                        IDBanco: IDBanco,
                        banco : banco
                    },
                    success: function(data){
                        if(data == "200"){
                            swal({
                                title: "Modificación Correcta!",
                                text: "Banco modificado correctamente.",
                                icon: "success",
                                button: "Aceptar",
                              });
                              tabla();
                              CancelaModificacion()
                        }
                        else{
                            swal({
                                title: "Modificación Fallida!",
                                text: "El banco no se pudo modificar.",
                                icon: "error",
                                button: "Aceptar",
                              });
                        }
                    }
                });
            }
        }
</script>
<% 
    String footer = request.getSession().getAttribute("footer").toString();
    out.println(footer);
%> 