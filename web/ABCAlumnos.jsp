<%-- 
    Document   : ABCAlumnos
    Created on : 16/09/2018, 09:01:34 PM
    Author     : Alienware
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
    String head = request.getSession().getAttribute("head").toString();
    out.println(head);
%>
<div class="content mt-3">
    <div class="row">
        <div class="col-12">
            <div class="col-6">
                <button 
                    type="button" 
                    class="btn-lg btn-success" 
                    onclick="InsertaAlumno()"
                    >Agregar Alumno <i class="fa fa-save"></i>
                </button>
                <button 
                    type="button" 
                    class="btn-lg btn-primary modificabtn" 
                    onclick="ModificaAlumno()"
                    style="display: none;"
                    >Modificar Alumno <i class="fa fa-male"></i>
                </button>
                <button 
                    type="button" 
                    class="btn-lg btn-danger modificabtn"
                    onclick="CancelaModificacion()"
                    style="display: none;"
                    >Cancelar Operación<i class="fa fa-times-circle"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="row">
        <div class="col-12">
            <div class="col-2">
                <div class="form-group">
                    <label for="usr">Primer Nombre:</label>
                    <input type="text" class="form-control form-control-sm nombresPersonas limpia" id="firstname">
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="usr">Segundo Nombre:</label>
                    <input type="text" class="form-control form-control-sm nombresPersonas limpia" id="secondname">
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="usr">Primer Apellido:</label>
                    <input type="text" class="form-control form-control-sm nombresPersonas limpia" id="firstlastname">
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="usr">Segundo Apellido:</label>
                    <input type="text" class="form-control form-control-sm nombresPersonas limpia" id="secondlastname">
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="usr">CUI:</label>
                    <input 
                        type="text" 
                        class="form-control form-control-sm number limpia" 
                        id="cui"
                        style="text-align: right;"
                    >
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="usr">Fecha de Nacimiento:</label>
                    <div class="input-group">
                        <input type="text" name="date" id="dateNew" class="form-control form-control-sm limpia" readonly >
                        <div class="input-group-addon">
                            <i class="fa fa-calendar" id="imgf1" style="cursor: pointer;"></i>
                        </div>
                    </div>	
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="col-2">
                <div class="form-group">
                    <label for="usr" class="">Código:</label>
                    <input 
                        type="text" 
                        class="form-control form-control-sm limpia" 
                        id="codigo"
                        style="text-align: right;"
                    >
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="usr" class="">Genero:</label>
                    <select type="text" class="form-control form-control-sm" id="genero"></select>
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="usr" class="">Teléfono:</label>
                    <input 
                        type="text" 
                        class="form-control form-control-sm number limpia" 
                        id="telefono" 
                        maxlength="8"
                        style="text-align: right;"
                    >
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="usr" class="">Contacto Emergencia:</label>
                    <input type="text" class="form-control form-control-sm nombresPersonas limpia" id="nombreemergencia">
                </div>
            </div>
            <div class="col-4">
                <div class="form-group">
                    <label for="usr" class="">Dirección:</label>
                    <input type="text" class="form-control form-control-sm limpia" id="direccion">
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 table-responsive" class="">
            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                <thead class="thead-dark">
                  <tr>
                    <th>Código</th>
                    <th>Nombre Completo</th>
                    <th>Dirección</th>
                    <th>Teléfono</th>
                    <th>CUI</th>
                    <th>Fecha Nacimiento</th>
                    <th>Contacto Emergencia</th>
                    <th>Estado</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody id="tbodyAlumno"></tbody>
            </table>    
        </div>
    </div>
    <div class="row">
    </div>
</div>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function(){
        <%String tipo = request.getSession().getAttribute("genero").toString();%>
        <%String tbodyAlumno = request.getSession().getAttribute("tbodyAlumno").toString();%>
        var tipoGenero = "<% out.print(tipo);%>";
        var tbodyAlumno = "<% out.print(tbodyAlumno);%>";
        var IDAlumnoMod = 0;
        $('.nombresPersonas').keyup(function (){
            this.value = (this.value + '').replace(/([^ a-z-A-z])/g, '');
        });
        $('.number').keyup(function (){
            this.value = (this.value + '').replace(/([^0-9])/g, '');
        });
        $('#firstname').focus();    
        $("#genero").html(tipoGenero);
        $("#tbodyAlumno").html(tbodyAlumno);
        $('#imgf1').click(function(){
            $( "#dateNew" ).datepicker( "show" );
        });
        eliminarAlumno();
        EditaAlumno();
        $("#dateNew").datepicker({
            format: " dd-M-yyyy",             
            autoclose: true,
	    language: 'es'
        })
    });
    
    function ValidaCampos(PrimerNombre,PrimerApellido,CUI,fechaNa,codigo,Genero,telefono,nombreEmergencia,direccion){
        var dato=false;
        if(PrimerNombre == null || PrimerNombre == ''){
            swal({
               title: "Faltan datos por ingresar!",
               text: "Ingrese el primer nombre del alumno.",
               icon: "info",
               button: "Aceptar"
             }).then(() => {
               $("#firstname").focus();
             });
        }else if(PrimerApellido == null || PrimerApellido == ''){
            swal({
               title: "Faltan datos por ingresar!",
               text: "Ingrese el primer apellido del alumno.",
               icon: "info",
               button: "Aceptar"
             }).then(() => {
               $("#firstlastname").focus();
             });
        }else if(CUI == null || CUI == ''){
            swal({
               title: "Faltan datos por ingresar!",
               text: "Ingrese el CUI del alumno.",
               icon: "info",
               button: "Aceptar"
             }).then(() => {
               $("#cui").focus();
            });
        }else if(fechaNa == null || fechaNa == ''){
            swal({
               title: "Faltan datos por ingresar!",
               text: "Ingrese la fecha de nacimiento del alumno.",
               icon: "info",
               button: "Aceptar"
            }).then(() => {
               $("#dateNew").focus();
            });
        }else if(codigo == null || codigo == ''){
            swal({
               title: "Faltan datos por ingresar!",
               text: "Ingrese el código del alumno.",
               icon: "info",
               button: "Aceptar"
             }).then(() => {
               $("#codigo").focus();
             });
        }else if(Genero == 0){
            swal({
               title: "Faltan datos por ingresar!",
               text: "Seleccione el genero del alumno.",
               icon: "info",
               button: "Aceptar"
            }).then(() => {
               $("#genero").focus();
            });
        }else if(telefono == null || telefono == ''){
            swal({
               title: "Faltan datos por ingresar!",
               text: "Ingrese el teléfono de contacto.",
               icon: "info",
               button: "Aceptar"
            }).then(() => {
               $("#telefono").focus();
            });
        }else if(nombreEmergencia == null || nombreEmergencia == ''){
            swal({
               title: "Faltan datos por ingresar!",
               text: "Ingrese el nombre de un responsable del alumno.",
               icon: "info",
               button: "Aceptar"
            }).then(() => {
               $("#nombreemergencia").focus();
            });
        }else if(direccion == null || direccion == ''){
            swal({
               title: "Faltan datos por ingresar!",
               text: "Ingrese la dirección del alumno.",
               icon: "info",
               button: "Aceptar"
            }).then(() => {
               $("#direccion").focus();
            });
        }else{
            dato=true;
        }
        return dato;
    }
    
    function InsertaAlumno(){
        var PrimerNombre = $("#firstname").val();
        var SegundoNombre = $("#secondname").val();
        var PrimerApellido = $("#firstlastname").val();
        var SegundoApellido = $("#secondlastname").val();
        var CUI = $("#cui").val();
        var fechaNa = $("#dateNew").val();
        var codigo = $("#codigo").val();
        var Genero = $("#genero").val();
        var telefono = $("#telefono").val();
        var nombreEmergencia = $("#nombreemergencia").val();
        var direccion = $("#direccion").val();
        
        var valida = ValidaCampos(PrimerNombre,PrimerApellido,CUI,fechaNa,codigo,Genero,telefono,nombreEmergencia,direccion);
        
        if(valida == true){
            $.ajax({
                type: "POST",
                url: "ABCAlumnosAjax",
                data: {
                    f:1, 
                    CUI : CUI,
                    codigo : codigo,
                    Genero : Genero,
                    fechaNa : fechaNa,
                    telefono : telefono,
                    direccion : direccion,
                    PrimerNombre : PrimerNombre, 
                    SegundoNombre : SegundoNombre, 
                    PrimerApellido : PrimerApellido,
                    SegundoApellido : SegundoApellido,
                    nombreEmergencia : nombreEmergencia 

                },
                success: function(data){
                    if(data == "200"){
                        swal({
                            title: "Ingreso Correcto!",
                            text: "Alumno Ingresado Correctamente.",
                            icon: "success",
                            button: "Aceptar",
                          });
                          tabla();
                    }
                    else{
                        swal({
                            title: "Ingreso Fallido!",
                            text: "Alumno no se pudo ingresar.",
                            icon: "error",
                            button: "Aceptar",
                          });
                    }
                }
            });
        }
    }
    
    function tabla(){
        $.ajax({
                type: "POST",
                url: "ABCAlumnosAjax",
                data: {f:2},
                success: function(data){
                    $('.limpia').val('');
                    $('#genero').val(0);
                    $("#tbodyAlumno").html(data);
                    eliminarAlumno();
                    EditaAlumno();
                }
            });
    }
    function eliminarAlumno(){
            $('.elimina').click(function(e){
                var id = e.target.id;
                swal({
                        title: "Esta seguro de eliminar este alumno?",
                        text: "Toda la información referente a este alumno sera borrada",
                        icon: "warning",
                        buttons: true,
                        buttons: ["Cancelar", "Aceptar"],
                        dangerMode: true,
                        
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: "POST",
                            url: "ABCAlumnosAjax",
                            data: {f:3, id: id},
                            success: function(data){
                                if(data == "200"){
                                    swal("Alumno eliminado correctamente!", {
                                        icon: "success",
                                        button: "Aceptar",
                                    });
                                      tabla();
                                }
                                else{
                                    swal({
                                        title: "Operación Fallida!",
                                        text: "Alumno no se pudo eliminar.",
                                        icon: "error",
                                        button: "Aceptar",
                                      });
                                }
                            }
                        });
                    } else {
                        swal({
                            text: "Operación cancelada!",
                            button: "Aceptar",
                        });
                    }
                });
            })
        }
        
        function EditaAlumno(){
            $('.modifica').click(function(e){
                $('.btn-success').hide();
                $('.modificabtn').show();
                var id = e.target.id;
                var nombre1 = $('#'+id).data('nombre1');
                var nombre2 = $('#'+id).data('nombre2');
                var apellido1 = $('#'+id).data('apellido1');
                var apellido2 = $('#'+id).data('apellido2');
                var CUI = $('#'+id).data('cui');
                var fecha = $.trim($('#'+id).data('fecha'));
                var codigo = $('#'+id).data('codigo');
                var genero = $('#'+id).data('genero');
                var nombre1 = $('#'+id).data('nombre1');
                var telefono = $('#'+id).data('telefono');
                var emergencia = $('#'+id).data('emergencia');
                var direccion = $('#'+id).data('direccion');
                IDAlumnoMod = $('#'+id).data('idelumno');
                
                $("#firstname").val(nombre1);
                $("#secondname").val(nombre2);
                $("#firstlastname").val(apellido1);
                $("#secondlastname").val(apellido2);
                $("#cui").val(CUI);
                $("#dateNew").val(fecha);
                $("#codigo").val(codigo);
                $("#genero").val(genero);
                $("#telefono").val(telefono);
                $("#nombreemergencia").val(emergencia);
                $("#direccion").val(direccion);
            })
        }
        
        function CancelaModificacion(){
            $('.limpia').val('');
            $('#genero').val(0);
            $('.btn-success').show();
            $('.modificabtn').hide();
        }
        
        function ModificaAlumno(){
            var PrimerNombre = $("#firstname").val();
            var SegundoNombre = $("#secondname").val();
            var PrimerApellido = $("#firstlastname").val();
            var SegundoApellido = $("#secondlastname").val();
            var CUI = $("#cui").val();
            var fechaNa = $("#dateNew").val();
            var codigo = $("#codigo").val();
            var Genero = $("#genero").val();
            var telefono = $("#telefono").val();
            var nombreEmergencia = $("#nombreemergencia").val();
            var direccion = $("#direccion").val();

            var valida = ValidaCampos(PrimerNombre,PrimerApellido,CUI,fechaNa,codigo,Genero,telefono,nombreEmergencia,direccion);

            if(valida == true){
                $.ajax({
                    type: "POST",
                    url: "ABCAlumnosAjax",
                    data: {
                        f:4,
                        ID: IDAlumnoMod,
                        CUI : CUI,
                        codigo : codigo,
                        Genero : Genero,
                        fechaNa : fechaNa,
                        telefono : telefono,
                        direccion : direccion,
                        PrimerNombre : PrimerNombre, 
                        SegundoNombre : SegundoNombre, 
                        PrimerApellido : PrimerApellido,
                        SegundoApellido : SegundoApellido,
                        nombreEmergencia : nombreEmergencia 

                    },
                    success: function(data){
                        if(data == "200"){
                            swal({
                                title: "Modificación Correcta!",
                                text: "Alumno modificado correctamente.",
                                icon: "success",
                                button: "Aceptar",
                              });
                              tabla();
                              CancelaModificacion()
                        }
                        else{
                            swal({
                                title: "Modificación Fallida!",
                                text: "Alumno no se pudo modificar.",
                                icon: "error",
                                button: "Aceptar",
                              });
                        }
                    }
                });
            }
        }
</script>
<% 
    String footer = request.getSession().getAttribute("footer").toString();
    out.println(footer);
%>  