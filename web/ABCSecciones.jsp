<%-- 
    Document   : ABSsecciones
    Created on : 30/09/2018, 07:10:57 PM
    Author     : Alienware
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% 
    String head = request.getSession().getAttribute("head").toString();
    out.println(head);
%>
<div class="content mt-3">
    <div class="row">
        <div class="col-12">
            <div class="col-6">
                <button 
                    type="button" 
                    class="btn-lg btn-success" 
                    onclick="InsertaClase()"
                    >Agregar Sección <i class="fa fa-save"></i>
                </button>
                <button 
                    type="button" 
                    class="btn-lg btn-primary modificabtn" 
                    onclick="ModificaSeccion()"
                    style="display: none;"
                    >Modificar Sección <i class="fa fa-male"></i>
                </button>
                <button 
                    type="button" 
                    class="btn-lg btn-danger modificabtn"
                    onclick="CancelaModificacion()"
                    style="display: none;"
                    >Cancelar Operación<i class="fa fa-times-circle"></i>
                </button>
            </div>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>
    <div class="row">
        <div class="col-12">
            <div class="col-2">
                <div class="form-group">
                    <label for="usr">Grado:</label>
                    <input type="text" class="form-control form-control-sm letras limpia" id="grado">
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="usr">Sección:</label>
                    <input type="text" class="form-control form-control-sm letras limpia" id="seccion" maxlength="1">
                </div>
            </div>
            <div class="col-2">
                <div class="form-group">
                    <label for="usr">Jornada:</label>
                    <select class="form-control form-control-sm nombresPersonas limpia" id="jornada">
                        <option value="0">Seleccione</option>
                        <option value="MATUTINO">Matutino</option>
                        <option value="VESPERTINO">Vespertino</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12 table-responsive" class="">
            <table id="bootstrap-data-table" class="table table-striped table-bordered">
                <thead class="thead-dark">
                  <tr>
                    <th>No.</th>
                    <th>Grado</th>
                    <th>Sección</th>
                    <th>Jornada</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody id="tbodySeccion"></tbody>
            </table>    
        </div>
    </div>
    <div class="row">
    </div>
</div>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(document).ready(function(){
        <%String body = request.getSession().getAttribute("tbodySeccion").toString();%>
        var cuerpotabla = "<% out.print(body);%>";
        $("#tbodySeccion").html(cuerpotabla);
        $('.letras').keyup(function (){
            this.value = (this.value + '').replace(/([^ a-z-A-z])/g, '').toUpperCase();
        });
        $('.numeros').keyup(function (){
            this.value = (this.value + '').replace(/([^0-9])/g, '');
        });
        $('#grado').focus();
        EliminarSeccion();
        EditaSeccion();
        IDASeccion=0;
    });
    
    function ValidaCampos(Grado,Seccion,Jornada){
        var dato=false;
        if(Grado == null || Grado == ''){
            swal({
               title: "Faltan datos por ingresar!",
               text: "Ingrese el grado.",
               icon: "info",
               button: "Aceptar"
             }).then(() => {
               $("#grado").focus();
             });
        }else if(Seccion == null || Seccion == ''){
            swal({
               title: "Faltan datos por ingresar!",
               text: "Ingrese la sección.",
               icon: "info",
               button: "Aceptar"
             }).then(() => {
               $("#seccion").focus();
             });
        }else if(Jornada == 0){
            swal({
               title: "Faltan datos por ingresar!",
               text: "Seleccione la jornada.",
               icon: "info",
               button: "Aceptar"
             }).then(() => {
               $("#jornada").focus();
            });
        }else{
            dato=true;
        }
        return dato;
    }
    
    function InsertaClase(){
        var Grado = $("#grado").val();
        var Seccion = $("#seccion").val();
        var Jornada = $("#jornada").val();
        
        var valida = ValidaCampos(Grado, Seccion, Jornada);
        
        if(valida == true){
            $.ajax({
                type: "POST",
                url: "ABCSeccionesAjax",
                data: {
                    f:1, 
                    Grado : Grado,
                    Seccion : Seccion,
                    Jornada : Jornada,
                },
                success: function(data){
                    if(data == "200"){
                        swal({
                            title: "Ingreso Correcto!",
                            text: "Sección Ingresado Correctamente.",
                            icon: "success",
                            button: "Aceptar",
                          });
                          tabla();
                    }
                    else{
                        swal({
                            title: "Ingreso Fallido!",
                            text: "La sección no se pudo ingresar.",
                            icon: "error",
                            button: "Aceptar",
                          });
                    }
                }
            });
        }
    }
    
    function tabla(){
        $.ajax({
                type: "POST",
                url: "ABCSeccionesAjax",
                data: {f:2},
                success: function(data){
                    $('.limpia').val('');
                    $('#jornada').val(0);
                    $("#tbodySeccion").html(data);
                    EliminarSeccion();
                    EditaSeccion();
                }
            });
    }
    function EliminarSeccion(){
            $('.elimina').click(function(e){
                var id = e.target.id;
                swal({
                        title: "Esta seguro de eliminar esta Sección?",
                        text: "Toda la información referente a este sección sera borrada",
                        icon: "warning",
                        buttons: true,
                        buttons: ["Cancelar", "Aceptar"],
                        dangerMode: true,
                        
                }).then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: "POST",
                            url: "ABCSeccionesAjax",
                            data: {f:3, id: id},
                            success: function(data){
                                if(data == "200"){
                                    swal("Sección eliminada correctamente!", {
                                        icon: "success",
                                        button: "Aceptar",
                                    });
                                      tabla();
                                }
                                else{
                                    swal({
                                        title: "Operación Fallida!",
                                        text: "La sección no se pudo eliminar.",
                                        icon: "error",
                                        button: "Aceptar",
                                      });
                                }
                            }
                        });
                    } else {
                        swal({
                            text: "Operación cancelada!",
                            button: "Aceptar",
                        });
                    }
                });
            })
        }
        
        function EditaSeccion(){
            $('.modifica').click(function(e){
                $('.btn-success').hide();
                $('.modificabtn').show();
                var id = e.target.id;
                var Grado = $('#'+id).data('grado');
                var Seccion = $('#'+id).data('seccion');
                var Jornada = $('#'+id).data('jornada');
                IDASeccion = $('#'+id).data('idsalon');
                
                $("#grado").val(Grado);
                $("#seccion").val(Seccion);
                $("#jornada").val(Jornada);
            })
        }
        
        function CancelaModificacion(){
            $('.limpia').val('');
            $('#genero').val(0);
            $('.btn-success').show();
            $('.modificabtn').hide();
        }
        
        function ModificaSeccion(){
            var grado = $("#grado").val();
            var seccion = $("#seccion").val();
            var jornada = $("#jornada").val();
            
            var valida = ValidaCampos(grado,seccion,jornada);

            if(valida == true){
                $.ajax({
                    type: "POST",
                    url: "ABCSeccionesAjax",
                    data: {
                        f:4,
                        IDsec: IDASeccion,
                        grado : grado,
                        seccion : seccion,
                        jornada : jornada
                    },
                    success: function(data){
                        if(data == "200"){
                            swal({
                                title: "Modificación Correcta!",
                                text: "Sección modificada correctamente.",
                                icon: "success",
                                button: "Aceptar",
                              });
                              tabla();
                              CancelaModificacion()
                        }
                        else{
                            swal({
                                title: "Modificación Fallida!",
                                text: "La sección no se pudo modificar.",
                                icon: "error",
                                button: "Aceptar",
                              });
                        }
                    }
                });
            }
        }
</script>
<% 
    String footer = request.getSession().getAttribute("footer").toString();
    out.println(footer);
%>  