/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AJAX;

import Modelo.ABCSeccionesModelo;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Alienware
 */
@WebServlet(name = "ABCSeccionesAjax", urlPatterns = {"/ABCSeccionesAjax"})
public class ABCSeccionesAjax extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        ABCSeccionesModelo info = new ABCSeccionesModelo();
        String idcase = request.getParameter("f");
        boolean dato = false;
        int f = Integer.parseInt(idcase);
        String respuesta = "400";
        
        switch (f){
            case 1:
                String  Grado   = request.getParameter("Grado");
                String  Seccion   = request.getParameter("Seccion");
                String  Jornada   = request.getParameter("Jornada");

                dato = info.IngresaSeccion(
                                            Grado,
                                            Seccion,
                                            Jornada
                                        );
                if(dato == true)
                    respuesta = "200";

                response.getWriter().print(respuesta);
            break;
                
                case 2:
                    String tbodyTable = info.TablaSeccion();
                    response.getWriter().print(tbodyTable);
                break;
                
                case 3:
                    String  IDSeccion  = request.getParameter("id");
                    boolean EliminaSeccion = info.EliminaSeccion(IDSeccion);
                    if(EliminaSeccion == true)
                        respuesta = "200";
                    response.getWriter().print(respuesta);
                break;
                
                case 4:
                    String  IDsec   = request.getParameter("IDsec");
                    String  grado   = request.getParameter("grado");
                    String  seccion   = request.getParameter("seccion");
                    String  jornada   = request.getParameter("jornada");
                    
                    dato = info.ModificaSeccion(
                                                IDsec,
                                                grado,
                                                seccion,
                                                jornada
                                            );
                    if(dato == true)
                        respuesta = "200";

                    response.getWriter().print(respuesta);
                break;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
