/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AJAX;

import Modelo.ABCAlumnosModelo;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Alienware
 */
@WebServlet(name = "ABCAlumnosAjax", urlPatterns = {"/ABCAlumnosAjax"})
public class ABCAlumnosAjax extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            response.setContentType("text/html;charset=UTF-8");
            ABCAlumnosModelo info = new ABCAlumnosModelo();
            String idcase = request.getParameter("f");
            boolean dato = false;
            int f = Integer.parseInt(idcase);
            String respuesta = "400";
            
            switch (f){
                case 1:
                    String  CUI   = request.getParameter("CUI");
                    String  codigo   = request.getParameter("codigo");
                    String  Genero   = request.getParameter("Genero");
                    String  fechaNa   = request.getParameter("fechaNa");
                    String  telefono   = request.getParameter("telefono");
                    String  direccion   = request.getParameter("direccion");
                    String  PrimerNombre   = request.getParameter("PrimerNombre");
                    String  SegundoNombre   = request.getParameter("SegundoNombre");
                    String  PrimerApellido   = request.getParameter("PrimerApellido");
                    String  SegundoApellido   = request.getParameter("SegundoApellido");
                    String  nombreEmergencia   = request.getParameter("nombreEmergencia");


                    dato = info.IngresaAlumno(
                                                CUI,
                                                codigo,
                                                Genero,
                                                fechaNa,
                                                telefono,
                                                direccion,
                                                PrimerNombre, 
                                                SegundoNombre, 
                                                PrimerApellido,
                                                SegundoApellido,
                                                nombreEmergencia
                                            );
                    if(dato == true)
                        respuesta = "200";

                    response.getWriter().print(respuesta);
                break;
                
                case 2:
                    String tbodyTable = info.TablaAlumno();
                    response.getWriter().print(tbodyTable);
                break;
                
                case 3:
                    String  IDAlumno   = request.getParameter("id");
                    boolean EliminaAlumnoS = info.EliminaAlumno(IDAlumno);
                    if(EliminaAlumnoS == true)
                        respuesta = "200";
                    response.getWriter().print(respuesta);
                break;
                
                case 4:
                    String  CUImod   = request.getParameter("CUI");
                    String  IDAlumnomod   = request.getParameter("ID");
                    String  codigomod   = request.getParameter("codigo");
                    String  Generomod   = request.getParameter("Genero");
                    String  fechaNamod   = request.getParameter("fechaNa");
                    String  telefonomod   = request.getParameter("telefono");
                    String  direccionmod   = request.getParameter("direccion");
                    String  PrimerNombremod   = request.getParameter("PrimerNombre");
                    String  SegundoNombremod   = request.getParameter("SegundoNombre");
                    String  PrimerApellidomod   = request.getParameter("PrimerApellido");
                    String  SegundoApellidomod   = request.getParameter("SegundoApellido");
                    String  nombreEmergenciamod   = request.getParameter("nombreEmergencia");
                    
                    dato = info.ModificaAlumno(
                                                CUImod,
                                                codigomod,
                                                Generomod,
                                                fechaNamod,
                                                telefonomod,
                                                direccionmod,
                                                PrimerNombremod, 
                                                SegundoNombremod, 
                                                PrimerApellidomod,
                                                SegundoApellidomod,
                                                nombreEmergenciamod,
                                                IDAlumnomod
                                            );
                    if(dato == true)
                        respuesta = "200";

                    response.getWriter().print(respuesta);
                break;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
