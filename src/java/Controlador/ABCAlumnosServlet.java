/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

//import CRUD.GeneroJpaController;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Alienware
 */
@WebServlet(name = "ABCAlumnosServlet", urlPatterns = {"/ABCAlumnos"})
public class ABCAlumnosServlet extends HttpServlet {
    //GeneroJpaController genero = new GeneroJpaController();
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                MainControler pageWeb = new MainControler();
                pageWeb.setPageName("ABC Alumnos");
                String page = pageWeb.getPage("ABC de Alumnos");
                String footer = pageWeb.getFooter();
                String inputGenero = this.ListaGenero();
                String tbodyAlumno = this.TablaAlumno();
                response.setContentType("text/html;charset=UTF-8");
                request.getSession().setAttribute("head", page);
                request.getSession().setAttribute("footer", footer);
                request.getSession().setAttribute("genero", inputGenero);
                request.getSession().setAttribute("tbodyAlumno", tbodyAlumno);
                request.getRequestDispatcher("ABCAlumnos.jsp").forward(request, response);
    }
    
    private String ListaGenero(){
        String inputGenero = "<option value='0'>Seleccione Genero</option>";
        Conectiondb dato = new Conectiondb();
        String query ="SELECT id_genero, descrip FROM genero ORDER BY descrip";
        
        try {
            dato.conectar(); 
            ResultSet read = dato.consultar(query);
            while(read.next()){
               inputGenero += "<option value='"+read.getObject(1).toString()+"'>"+read.getObject(2).toString().toLowerCase()+"</option>";
            }
            read.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conectiondb.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return inputGenero;
    }
    
    private String TablaAlumno(){
        String tbodyAlumno = "";
        Conectiondb dato = new Conectiondb();
        String query ="SELECT * FROM alumnos";
        
        try {
            dato.conectar(); 
            ResultSet read = dato.consultar(query);
            while(read.next()){
                tbodyAlumno +=   "<tr>"+
                                "   <td style='text-align: right;'>"+read.getObject(2).toString()+"</td>"+
                                "   <td>"+read.getObject(3).toString()+"</td>"+
                                "   <td>"+read.getObject(4).toString()+"</td>"+
                                "   <td style='text-align: right;'>"+read.getObject(5).toString()+"</td>"+
                                "   <td style='text-align: right;'>"+read.getObject(6).toString()+"</td>"+
                                "   <td>"+read.getObject(7).toString()+"</td>"+
                                "   <td>"+read.getObject(8).toString()+"</td>"+
                                "   <td>"+read.getObject(9).toString()+"</td>"+
                                "   <td>"+
                                "       <i class='fa fa-edit modifica' style='Cursor:pointer'"+
                                "           data-nombre1='"+read.getObject(11).toString()+"'"+
                                "           data-nombre2='"+read.getObject(12).toString()+"'"+
                                "           data-apellido1='"+read.getObject(13).toString()+"'"+   
                                "           data-apellido2='"+read.getObject(14).toString()+"'"+
                                "           data-cui='"+read.getObject(6).toString()+"'"+
                                "           data-fecha='"+read.getObject(7).toString()+"'"+
                                "           data-codigo='"+read.getObject(2).toString()+"'"+
                                "           data-genero='"+read.getObject(15).toString()+"'"+
                                "           data-telefono='"+read.getObject(5).toString()+"'"+
                                "           data-emergencia='"+read.getObject(8).toString()+"'"+
                                "           data-direccion='"+read.getObject(4).toString()+"'"+
                                "           data-idelumno='"+read.getObject(1).toString()+"'"+
                                "           id='UPD"+read.getObject(1).toString()+"'"+
                                "></i>"+
                                "       <i "+
                                "           title='Eliminar Alumno' "+
                                "           class='fa fa-eraser elimina' "+
                                "           style='Cursor:pointer' id='"+read.getObject(1).toString()+
                                "'></i>"+
                                "   </td>"+
                                    
                                "</tr>";
            }
            read.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conectiondb.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return tbodyAlumno;
    }
    
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
