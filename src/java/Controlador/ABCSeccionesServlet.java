/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.ABCSeccionesModelo;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Alienware
 */
@WebServlet(name = "ABCSeccionesServlet", urlPatterns = {"/ABCSecciones"})
public class ABCSeccionesServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            MainControler pageWeb = new MainControler();
            ABCSeccionesModelo Secciondb = new ABCSeccionesModelo();
            pageWeb.setPageName("ABC de Secciones");
            String page = pageWeb.getPage("ABC de Secciones");
            String footer = pageWeb.getFooter();
            String tbodySeccion = Secciondb.TablaSeccion();
            response.setContentType("text/html;charset=UTF-8");
            request.getSession().setAttribute("tbodySeccion", tbodySeccion);
            request.getSession().setAttribute("head", page);
            request.getSession().setAttribute("footer", footer);
            request.getRequestDispatcher("ABCSecciones.jsp").forward(request, response);
    }
    
    private String ListaJornada(){
        String inputGenero = "<option value='0'>Seleccione Genero</option>";
        Conectiondb dato = new Conectiondb();
        String query ="SELECT id_JORNADA, descripcion FROM JORNADA ORDER BY descripcion";
        
        try {
            dato.conectar(); 
            ResultSet read = dato.consultar(query);
            while(read.next()){
               inputGenero += "<option value='"+read.getObject(1).toString()+"'>"+read.getObject(2).toString().toLowerCase()+"</option>";
            }
            read.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conectiondb.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return inputGenero;
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
