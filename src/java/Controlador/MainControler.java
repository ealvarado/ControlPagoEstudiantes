/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

/**
 *
 * @author Alienware
 */
public class MainControler {
    private String hade = "";
    private String pageName = "";
    private String PanelLeft = ""; 
    private String Page = "";
    private String RightHead = "";
    private String Footer = "";
    
    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getHade() {
        this.hade = "<!DOCTYPE html>\n" +
                    "<html>\n" +
                    "    <head>\n" +
                    "        <meta charset=\"utf-8\">\n" +
                    "        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n" +
                    "        <title>"+this.pageName+"</title>\n" +
                    "        <meta name=\"description\" content=\""+this.pageName+"\">\n" +
                    "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n" +
                    "        <link rel=\"apple-touch-icon\" href=\"apple-icon.png\">\n" +
                    "        <link rel=\"shortcut icon\" href=\"favicon.ico\">\n" +
                    "        <link rel=\"stylesheet\" href=\"lib/assets/css/normalize.css\">\n" +
                    "        <link rel=\"stylesheet\" href=\"lib/assets/css/bootstrap.min.css\">\n" +
                    "        <link rel=\"stylesheet\" href=\"lib/assets/css/font-awesome.min.css\">\n" +
                    "        <link rel=\"stylesheet\" href=\"lib/assets/css/themify-icons.css\">\n" +
                    "        <link rel=\"stylesheet\" href=\"lib/assets/css/flag-icon.min.css\">\n" +
                    "        <link rel=\"stylesheet\" href=\"lib/assets/css/cs-skin-elastic.css\">\n" +
                    "        <link rel=\"stylesheet\" href=\"lib/assets/css/lib/datatable/dataTables.bootstrap.min.css\">"+
                    "        <link rel=\"stylesheet\" href=\"lib/assets/scss/style.css\">\n" +
                    "        <link href=\"lib/assets/css/lib/vector-map/jqvmap.min.css\" rel=\"stylesheet\">\n" +
                    "        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>\n" +
                
                
                    
                    
                
                
                
                    "        <script src=\"lib/assets/js/vendor/jquery-1.11.3.min.js\"></script>\n" +
                    "        <script type=\"text/javascript\" src=\"lib/assets/js/bootstrap.min.js\"></script>"+
                
                
                    
                
                    
                    "        <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js\"></script>\n" +
                    "        <script src=\"lib/assets/js/plugins.js\"></script>\n" +
                    "        <script src=\"lib/assets/js/main.js\"></script>\n" +
                    "        <script src=\"lib/assets/js/lib/vector-map/jquery.vmap.sampledata.js\"></script>\n" +
                
                
                
                    "        <script src=\"lib/assets/js/jquery.js\"></script>"+
                    
                    "        <link href=\"lib/assets/css/bootstrap-datepicker3.min.css\" rel=\"stylesheet\" type=\"text/css\" />"+
                    
                    "        <script src=\"lib/assets/js/bootstrap-datepicker.min.js\" type=\"text/javascript\" /> </script>"+
                    "        <script src=\"lib/assets/js/bootstrap-datepicker.es.min.js\" type=\"text/javascript\" /> </script>"+
                    "    </head>";
        return hade;
    }
    
    public String getRightHead(String namePage){
        this.RightHead = "<div id=\"right-panel\" class=\"right-panel\">\n" +
                        "        <!-- Header-->\n" +
                        "        <header id=\"header\" class=\"header\" style=\"background-color: #E0E0E0;\">\n" +
                        "            <div class=\"header-menu\">\n" +
                        "                <div class=\"col-sm-7\">\n" +
                        "                    <a id=\"menuToggle\" class=\"menutoggle pull-left\"><i class=\"fa fa fa-tasks\"></i></a>\n" +
                        "                    <div class=\"header-left\">\n" +
                        "                        \n" +
                        "                    </div>\n" +
                        "                </div>\n" +
                        "                <div class=\"col-sm-5\">\n" +
                        "                    <div class=\"user-area dropdown float-right\">\n" +
                        "                        <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n" +
                        "                            <img class=\"user-avatar rounded-circle\" src=\"lib/images/userlog.png\" alt=\"User Avatar\">\n" +
                        "                        </a>\n" +
                        "                        <div class=\"user-menu dropdown-menu\">\n" +
                        "                                <a class=\"nav-link\" href=\"#\"><i class=\"fa fa- user\"></i>My Profile</a>\n" +
                        "                                <a class=\"nav-link\" href=\"#\"><i class=\"fa fa- user\"></i>Notifications <span class=\"count\">13</span></a>\n" +
                        "                                <a class=\"nav-link\" href=\"#\"><i class=\"fa fa -cog\"></i>Settings</a>\n" +
                        "                                <a class=\"nav-link\" href=\"#\"><i class=\"fa fa-power -off\"></i>Salir</a>\n" +
                        "                        </div>\n" +
                        "                    </div>\n" +
                        "                    <div class=\"language-select dropdown\" id=\"language-select\">\n" +
                        "                        <a class=\"dropdown-toggle\"  id=\"language\" aria-haspopup=\"true\" aria-expanded=\"true\">\n" +
                        "                            <i class=\"flag-icon flag-icon-gt\"></i>\n" +
                        "                        </a>\n" +
                        "                    </div>\n" +
                        "                </div>\n" +
                        "            </div>\n" +
                        "        </header><!-- /header -->\n" +
                        "        <!-- Header-->"+
                        "<div class=\"breadcrumbs\">\n" +
                        "            <div class=\"col-sm-6\">\n" +
                        "                <div class=\"page-header float-left\">\n" +
                        "                    <div class=\"page-title\">\n" +
                        "                        <h3>SOFTEDUC|\n" +
                        "                            <small>"+namePage+" <i class=\"fa fa-info-circle\" style=\"cursor: pointer;\" id='manual' data-toggle=\"tooltip\" title=\"Manual de Usuario\"></i></small>\n" +
                        "                        </h3>\n" +
                        "                    </div>\n" +
                        "                </div>\n" +
                        "            </div>\n" +
                        "            <div class=\"col-sm-6\">\n" +
                        "                <div class=\"page-header float-right\">\n" +
                        "                    <div class=\"page-title\">\n" +
                        "                        <ol class=\"breadcrumb text-right\">\n" +
                        "                            <li class=\"active\">SOFTEDUC</li>\n" +
                        "                        </ol>\n" +
                        "                    </div>\n" +
                        "                </div>\n" +
                        "            </div>\n" +
                        "        </div>";
        return this.RightHead;
    }
    
    public String getPanelLeft(){
        this.PanelLeft = "    <body class=\"open\">\n" +
                        "        <!-- Left Panel -->\n" +
                        "        <aside id=\"left-panel\" class=\"left-panel\">\n" +
                        "            <nav class=\"navbar navbar-expand-sm navbar-default\">\n" +
                        "                <div class=\"navbar-header\">\n" +
                        "                    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#main-menu\" aria-controls=\"main-menu\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n" +
                        "                        <i class=\"fa fa-bars\"></i>\n" +
                        "                    </button>\n" +
                        "                    <a class=\"navbar-brand\" href=\"MainMenu\"><img src=\"lib/images/icons/SOFTEDUC.png\" alt=\"Logo\"></a>\n" +
                        "                    <a class=\"navbar-brand hidden\" href=\"MainMenu\"><img src=\"lib/images/icons/SOFTEDUC.png\" alt=\"Logo\"></a>\n" +
                        "                </div>\n" +
                        "                <div id=\"main-menu\" class=\"main-menu collapse navbar-collapse\">\n" +
                        "                    <ul class=\"nav navbar-nav\">\n" +
                        "                        <li class=\"active\">\n" +
                        "                            <a href=\"index.html\"> <i class=\"menu-icon fa fa-dashboard\"></i>Dashboard </a>\n" +
                        "                        </li>\n" +
                        "                        <h3 class=\"menu-title\">Procesos</h3><!-- /.menu-title -->\n" +
                        "                        <li class=\"menu-item-has-children dropdown\">\n" +
                        "                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> <i class=\"menu-icon fa fa-cogs\"></i>Mantenimiento</a>\n" +
                        "                            <ul class=\"sub-menu children dropdown-menu\">\n" +
                        "                                <li><i class=\"fa fa-users\"></i><a href=\"ABCAlumnos\">ABC de Alumnos</a></li>\n" +
                        "                                <li><i class=\"fa fa fa-desktop\"></i><a href=\"ABCSecciones\">ABC de Secciones</a></li>\n" +
                        "                                <li><i class=\"fa fa fa-money\"></i><a href=\"ABCBancos\">ABC de Bancos</a></li>\n" +
                        "                                <li><i class=\"fa fa fa-credit-card\"></i><a href=\"MainMenu\">ABC de Pagos</a></li>\n" +
                        "                            </ul>\n" +
                        "                        </li>\n" +
                        "                        <li class=\"menu-item-has-children dropdown\">\n" +
                        "                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> <i class=\"menu-icon fa fa-table\"></i>Procesos</a>\n" +
                        "                            <ul class=\"sub-menu children dropdown-menu\">\n" +
                        "                                <li><i class=\"fa fa-table\"></i><a href=\"MainMenu\">Proceso de Pago</a></li>\n" +
                        "                                <li><i class=\"fa fa-table\"></i><a href=\"MainMenu\">Secciones alumno</a></li>\n" +
                        "                            </ul>\n" +
                        "                        </li>\n" +
                        "                        <li class=\"menu-item-has-children dropdown\">\n" +
                        "                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\"> <i class=\"menu-icon fa fa-bar-chart-o\"></i>Reportes</a>\n" +
                        "                            <ul class=\"sub-menu children dropdown-menu\">\n" +
                        "                                <li><i class=\"menu-icon fa fa-list-alt\"></i><a href=\"MainMenu\">Alumnos y Secciones</a></li>\n" +
                        "                                <li><i class=\"menu-icon fa fa-usd\"></i><a href=\"MainMenu\">Pagos</a></li>\n" +
                        "                            </ul>\n" +
                        "                        </li>\n" +
                        "                    </ul>\n" +
                        "                </div><!-- /.navbar-collapse -->\n" +
                        "            </nav>\n" +
                        "        </aside><!-- /#left-panel -->";
        return this.PanelLeft;
    }
    
    public String getFooter(){
        this.Footer = "</div><!-- /#right-panel -->\n" +
                    "    <!-- Right Panel -->\n" +
                    "  </body>\n" +
                    "</html>";
        return this.Footer;
    }
    
    public String getPage(String namepage){
        String encabezado = this.getHade();
        String PanelLeftL = this.getPanelLeft();
        String rightPanelL = this.getRightHead(namepage);
        this.Page = encabezado+PanelLeftL+rightPanelL;
        return this.Page;
    }
    
}
