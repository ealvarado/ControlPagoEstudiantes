/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.sql.DriverManager;
import java.sql.ResultSet;
import javax.jms.Connection;
import java.sql.SQLException;
import java.sql.Statement;
/**
 *
 * @author Alienware
 */
public class Conectiondb {
    private static Connection conn;
    private static final String driver = "com.mysql.jdbc.Driver";
    private static final String user = "root";
    private static final String pass = "1990ea";
    private static final String url = "jdbc:mysql://localhost:3306/SOFTEDUC?zeroDateTimeBehavior=convertToNull";
    private java.sql.Connection conexion; 
     
    public java.sql.Connection getConexion() { 
        return conexion; 
    }    
    public void setConexion(java.sql.Connection conexion) { 
        this.conexion = conexion; 
    } 
    
    public Conectiondb conectar() { 
        try { 
            Class.forName(driver);  

            conexion = DriverManager.getConnection(url, user,pass);             
            if (conexion != null) { 
                System.out.println("Conexion exitosa!"); 
            } else { 
                System.out.println("Conexion fallida!"); 
            } 
        } catch (Exception e) { 
            e.printStackTrace(); 
        }        return this; 
    } 
    
    
    //public Conectiondb() { 
    //    conn = null;
    //    try {
    //        Class.forName(driver);
    //        conn = (Connection) DriverManager.getConnection(url, user,pass); 
    //        if(conn != null){
    //            System.out.println("conexion establecida");
    //        }
    //    } catch (ClassNotFoundException | SQLException e) {
    //       System.out.println("error al conectar: "+e);
    //    }
    //} 
    
    public boolean escribir(String sql) { 
        try { 
            Statement sentencia; 
            sentencia = getConexion().createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            sentencia.executeUpdate(sql);
            //sentencia.executeQuery(sql);
            //getConexion().commit(); 
            sentencia.close(); 
             
        } catch (SQLException e) { 
            e.printStackTrace(); 
            System.out.print("ERROR SQL "+e.getMessage()); 
            return false; 
        }         
        return true; 
    } 
    
    public ResultSet consultar(String sql) { 
        ResultSet resultado = null; 
        try { 
            Statement sentencia; 
            sentencia = getConexion().createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            resultado = sentencia.executeQuery(sql); 
             
        } catch (SQLException e) { 
            e.printStackTrace(); 
            return null; 
        }        return resultado; 
    }
}
