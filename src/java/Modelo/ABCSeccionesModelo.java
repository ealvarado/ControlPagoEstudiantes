/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Controlador.Conectiondb;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alienware
 */
public class ABCSeccionesModelo {
    public boolean IngresaSeccion(
                                    String grado,
                                    String seccion,
                                    String jornada
                                ){
        Conectiondb info = new Conectiondb();
        boolean respuesta = false;
        String qry = "INSERT SALON(GRADO,SECCION,JORNADA) " +
                     "VALUES('"+grado+"','"+seccion+"','"+jornada+"');";
        
        info.conectar();
        respuesta = info.escribir(qry);
        System.out.println(respuesta);
        return respuesta;
    }
    
    public String TablaSeccion(){
        String tbodySeccion = "";
        Conectiondb dato = new Conectiondb();
        String query ="SELECT * FROM SALON";
        
        try {
            dato.conectar(); 
            ResultSet read = dato.consultar(query);
            int numero=1;
            while(read.next()){
                tbodySeccion +=   "<tr>"+
                                  "   <td style='text-align: right;'>"+numero+"</td>"+
                                  "   <td>"+read.getObject(2).toString()+"</td>"+
                                  "   <td>"+read.getObject(3).toString()+"</td>"+
                                  "   <td>"+read.getObject(4).toString()+"</td>"+
                                  "   <td>"+
                                  "       <i class='fa fa-edit modifica' style='Cursor:pointer'"+
                                  "           title='Modificar Sección'   "+
                                  "           data-grado='"+read.getObject(2).toString()+"'"+
                                  "           data-seccion='"+read.getObject(3).toString()+"'"+
                                  "           data-jornada='"+read.getObject(4).toString()+"'"+   
                                  "           data-idsalon='"+read.getObject(1).toString()+"'"+
                                  "           id='UPD"+read.getObject(1).toString()+"'"+
                                  "       ></i>"+
                                  "       <i "+
                                  "           title='Eliminar Sección' "+
                                  "           class='fa fa-eraser elimina' "+
                                  "           style='Cursor:pointer' id='"+read.getObject(1).toString()+"' "+
                                  "       ></i>"+
                                  "   </td>"+                                    
                                  "</tr>";
                numero++;
            }
            read.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conectiondb.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return tbodySeccion;
    }
    
    public boolean ModificaSeccion(
                                    String ID,
                                    String grado,
                                    String seccion,
                                    String jornada
                                ){
        Conectiondb info = new Conectiondb();
        boolean respuesta = false;
        String qry =    "UPDATE SALON " +
                        "SET grado = '"+grado+"', " +
                        "seccion = '"+seccion+"', " +
                        "jornada = '"+jornada+"' " +
                        "WHERE idsalon ="+ID+"; ";
        
        info.conectar();
        respuesta = info.escribir(qry);
        System.out.println(respuesta);
        return respuesta;
    }
    
    public boolean EliminaSeccion(String ID){
        Conectiondb info = new Conectiondb();
        boolean respuesta = false;
        String qry = "DELETE FROM SALON WHERE idsalon="+ID;
        
        info.conectar();
        respuesta = info.escribir(qry);
        System.out.println(respuesta);
        return respuesta;
    }
}
