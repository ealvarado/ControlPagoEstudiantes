/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Controlador.Conectiondb;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alienware
 */
public class ABCAlumnosModelo {
    
    public boolean IngresaAlumno(
                                    String CUI,
                                    String codigo,
                                    String Genero,
                                    String fechaNa,
                                    String telefono,
                                    String direccion,
                                    String PrimerNombre, 
                                    String SegundoNombre, 
                                    String PrimerApellido,
                                    String SegundoApellido,
                                    String nombreEmergencia
                                ){
        Conectiondb info = new Conectiondb();
        boolean respuesta = false;
        String qry = "INSERT INTO ALUMNO(NOM_ALUM_1,NOM_ALUM_2,APE_ALUM_1,APE_ALUM_2,SEXO,CODIGO_ALUMNO,ALUMNOcol,DIRECCION, TEL_ALUM_EMER, \n" +
                    "			CUI_ALUM, FEC_NAC_ALUM,CONTACTO_EMERGENCIA,STS_ALUM)\n" +
                    "VALUES('"+PrimerNombre+"','"+SegundoNombre+"','"+PrimerApellido+"','"+SegundoApellido+"',"+Genero+","+
                    "       '"+codigo+"','','"+direccion+"','"+telefono+"','"+CUI+"','"+fechaNa+"','"+nombreEmergencia+"','ACT')";
        
        info.conectar();
        respuesta = info.escribir(qry);
        System.out.println(respuesta);
        return respuesta;
    }
    
    public String TablaAlumno(){
        String tbodyAlumno = "";
        Conectiondb dato = new Conectiondb();
        String query ="SELECT * FROM alumnos";
        
        try {
            dato.conectar(); 
            ResultSet read = dato.consultar(query);
            while(read.next()){
               tbodyAlumno +=   "<tr>"+
                                "   <td style='text-align: right;'>"+read.getObject(2).toString()+"</td>"+
                                "   <td>"+read.getObject(3).toString()+"</td>"+
                                "   <td>"+read.getObject(4).toString()+"</td>"+
                                "   <td style='text-align: right;'>"+read.getObject(5).toString()+"</td>"+
                                "   <td style='text-align: right;'>"+read.getObject(6).toString()+"</td>"+
                                "   <td>"+read.getObject(7).toString()+"</td>"+
                                "   <td>"+read.getObject(8).toString()+"</td>"+
                                "   <td>"+read.getObject(9).toString()+"</td>"+
                                "   <td>"+
                                "       <i class='fa fa-edit modifica' style='Cursor:pointer'"+
                                "           data-nombre1='"+read.getObject(11).toString()+"'"+
                                "           data-nombre2='"+read.getObject(12).toString()+"'"+
                                "           data-apellido1='"+read.getObject(13).toString()+"'"+   
                                "           data-apellido2='"+read.getObject(14).toString()+"'"+
                                "           data-cui='"+read.getObject(6).toString()+"'"+
                                "           data-fecha='"+read.getObject(7).toString()+"'"+
                                "           data-codigo='"+read.getObject(2).toString()+"'"+
                                "           data-genero='"+read.getObject(15).toString()+"'"+
                                "           data-telefono='"+read.getObject(5).toString()+"'"+
                                "           data-emergencia='"+read.getObject(8).toString()+"'"+
                                "           data-direccion='"+read.getObject(4).toString()+"'"+
                                "           data-idelumno='"+read.getObject(1).toString()+"'"+
                                "           id='UPD"+read.getObject(1).toString()+"'"+
                                "></i>"+
                                "       <i "+
                                "           title='Eliminar Alumno' "+
                                "           class='fa fa-eraser elimina' "+
                                "           style='Cursor:pointer' id='"+read.getObject(1).toString()+
                                "'></i>"+
                                "   </td>"+
                                    
                                "</tr>";
            }
            read.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conectiondb.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return tbodyAlumno;
    }
    
    public boolean EliminaAlumno(String ID){
        Conectiondb info = new Conectiondb();
        boolean respuesta = false;
        String qry = "DELETE FROM ALUMNO WHERE idalumno="+ID;
        
        info.conectar();
        respuesta = info.escribir(qry);
        System.out.println(respuesta);
        return respuesta;
    }
    
    public boolean ModificaAlumno(
                                    String CUI,
                                    String codigo,
                                    String Genero,
                                    String fechaNa,
                                    String telefono,
                                    String direccion,
                                    String PrimerNombre, 
                                    String SegundoNombre, 
                                    String PrimerApellido,
                                    String SegundoApellido,
                                    String nombreEmergencia,
                                    String IDAlumnomod
                                ){
        Conectiondb info = new Conectiondb();
        boolean respuesta = false;
        String qry =    " UPDATE ALUMNO " +
                        " SET NOM_ALUM_1 = '"+PrimerNombre+"', " +
                        " NOM_ALUM_2 = '"+SegundoNombre+"',APE_ALUM_1 = '"+PrimerApellido+"', " +
                        " APE_ALUM_2 = '"+SegundoApellido+"',SEXO = '"+Genero+"', " +
                        " CODIGO_ALUMNO = '"+codigo+"',ALUMNOcol = '', " +
                        " DIRECCION = '"+direccion+"', TEL_ALUM_EMER = '"+telefono+"', " +
                        " CUI_ALUM = '"+CUI+"', FEC_NAC_ALUM = '"+fechaNa+"', " +
                        " CONTACTO_EMERGENCIA = '"+nombreEmergencia+"' " +
                        " WHERE IDALUMNO = "+IDAlumnomod;
        
        info.conectar();
        respuesta = info.escribir(qry);
        System.out.println(respuesta);
        return respuesta;
    }
}
