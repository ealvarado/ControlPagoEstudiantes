/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Controlador.Conectiondb;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Alienware
 */
public class ABCBancosModelo {
    public boolean IngresaBanco(String banco){
        Conectiondb info = new Conectiondb();
        boolean respuesta = false;
        String qry = "INSERT INTO BANCO(NOMBRE) " +
                     "VALUES('"+banco+"');";
        
        info.conectar();
        respuesta = info.escribir(qry);
        System.out.println(respuesta);
        return respuesta;
    }
    
    public String TablaBanco(){
        String tbodySeccion = "";
        Conectiondb dato = new Conectiondb();
        String query ="SELECT * FROM BANCO";
        
        try {
            dato.conectar(); 
            ResultSet read = dato.consultar(query);
            int numero=1;
            while(read.next()){
                tbodySeccion +=   "<tr>"+
                                  "   <td style='text-align: right;'>"+numero+"</td>"+
                                  "   <td>"+read.getObject(2).toString()+"</td>"+
                                  "   <td>"+
                                  "       <i class='fa fa-edit modifica' style='Cursor:pointer'"+
                                  "           title='Modificar Banco'   "+
                                  "           data-banco='"+read.getObject(2).toString()+"'"+ 
                                  "           data-idbanco='"+read.getObject(1).toString()+"'"+
                                  "           id='UPD"+read.getObject(1).toString()+"'"+
                                  "       ></i>"+
                                  "       <i "+
                                  "           title='Eliminar Banco' "+
                                  "           class='fa fa-eraser elimina' "+
                                  "           style='Cursor:pointer' id='"+read.getObject(1).toString()+"' "+
                                  "       ></i>"+
                                  "   </td>"+                                    
                                  "</tr>";
                numero++;
            }
            read.close();
        } catch (SQLException ex) {
            Logger.getLogger(Conectiondb.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return tbodySeccion;
    }
    
    public boolean ModificaBanco(
                                    String ID,
                                    String banco
                                ){
        Conectiondb info = new Conectiondb();
        boolean respuesta = false;
        String qry =    "UPDATE BANCO " +
                        "SET NOMBRE = '"+banco+"' " +
                        "WHERE id_banco ="+ID+"; ";
        
        info.conectar();
        respuesta = info.escribir(qry);
        System.out.println(respuesta);
        return respuesta;
    }
    
    public boolean EliminaBanco(String ID){
        Conectiondb info = new Conectiondb();
        boolean respuesta = false;
        String qry = "DELETE FROM Banco WHERE id_banco="+ID;
        
        info.conectar();
        respuesta = info.escribir(qry);
        System.out.println(respuesta);
        return respuesta;
    }
}
